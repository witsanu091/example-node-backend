// src/utils/response.ts

export enum StatusCode {
    Success = 200,
    ServerError = 500,
}

export interface ResponseMessage {
    statusCode: StatusCode;
    developMessage: string;
}

export const responseSuccess: ResponseMessage = {
    statusCode: StatusCode.Success,
    developMessage: 'Success',
};

export const responseError: ResponseMessage = {
    statusCode: StatusCode.ServerError,
    developMessage: 'Failed',
};
