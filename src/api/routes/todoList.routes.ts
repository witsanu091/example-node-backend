
import { Router, Request, Response } from 'express';
import { TodoListController } from '../controller/todoList.controller';

const router = Router();

const todoListController = new TodoListController();

router.get('/todo-list', async (req: Request, res: Response) => {
    console.log("Start Route Get");
    const result = await todoListController.findAll()
    console.log("🚀  result:", result)
    return res.json(result);
});


router.get('/todo-list/by', async (req: Request, res: Response) => {
    const { id, date, status } = req.query;
    console.log("Start Route Get By Key");
    const result = await todoListController.findByKey({ id, date, status })
    console.log("🚀 result:", result)
    return res.json(result);
});


export default router;
