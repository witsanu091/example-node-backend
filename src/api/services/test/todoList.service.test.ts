import { TodoListService } from '../todoList.service';
import fs from 'fs/promises';

jest.mock('fs/promises');

describe('TodoListService', () => {
    let service: TodoListService;

    beforeEach(() => {
        service = new TodoListService({ id: '1', date: '2023-01-01', list: [] }, 'pending');
        jest.clearAllMocks();
    });

    describe('readFileJson', () => {
        it('should read JSON from file and return parsed data', async () => {
            const mockData = JSON.stringify([{ id: '1', date: '2023-01-01', list: [{ status: 'pending' }] }]);
            fs.readFile = jest.fn().mockResolvedValue(mockData);

            const result = await service.readFileJson();

            expect(fs.readFile).toBeCalled();
            expect(result).toEqual(JSON.parse(mockData));
        });

        it('should throw an error if the file cannot be read', async () => {
            fs.readFile = jest.fn().mockRejectedValue(new Error('File read error'));

            await expect(service.readFileJson()).rejects.toThrow('File read error');
        });
    });

    describe('getDataByKeyFromJson', () => {
        it('should filter data by id, date, and status', async () => {
            const mockData = JSON.stringify([
                { id: '1', date: '2023-01-01', list: [{ status: 'pending' }, { status: 'completed' }] },
                { id: '2', date: '2023-01-02', list: [{ status: 'pending' }] }
            ]);
            fs.readFile = jest.fn().mockResolvedValue(mockData);

            const result = await service.getDataByKeyFromJson();

            expect(fs.readFile).toBeCalled();
            expect(result).toEqual([{ id: '1', date: '2023-01-01', list: [{ status: 'pending' }] }]);
        });

        it('should handle cases where no filters match', async () => {
            const mockData = JSON.stringify([
                { id: '3', date: '2023-01-03', list: [{ status: 'completed' }] }
            ]);
            fs.readFile = jest.fn().mockResolvedValue(mockData);

            const result = await service.getDataByKeyFromJson();

            expect(result).toEqual([]);
        });

        it('should throw an error if the file cannot be read', async () => {
            fs.readFile = jest.fn().mockRejectedValue(new Error('File read error'));

            await expect(service.getDataByKeyFromJson()).rejects.toThrow('File read error');
        });
    });
});
