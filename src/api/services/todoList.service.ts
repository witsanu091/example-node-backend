import fs from "fs/promises";
import path from "path";
import { ToDoList } from "../models/todoList.models";
const todoListJson = path.join(__dirname, "..", "..", "data", "todoList.json");

export class TodoListService {
    id: string;
    date: string;
    status: string;


    constructor(ToDoList: ToDoList, status: string) {
        this.id = ToDoList.id;
        this.date = ToDoList.date;
        this.status = status;
    }
    async readFileJson() {
        try {
            console.log("service readFileJson");
            const result = await fs.readFile(todoListJson, "utf-8");
            console.log("🚀  TodoListService  result:", result)
            const jsonData = JSON.parse(result);
            console.log("jsonData:", jsonData);
            return jsonData;
        } catch (error: any) {
            console.log("🚀  TodoListService  error:", error)
            throw error;
        }
    }

    async getDataByKeyFromJson() {
        try {
            console.log("service getDataByKeyFromJson");
            const result = await fs.readFile(todoListJson, "utf-8");
            console.log("🚀  TodoListService  result:", result)
            let jsonData = JSON.parse(result);
            if (this.id) {
                jsonData = jsonData.filter((todo: any) => todo.id === this.id);
            }

            if (this.date) {
                jsonData = jsonData.filter((todo: any) => todo.date === this.date);
            }

            if (this.status !== undefined) {
                jsonData = jsonData.flatMap((todo: any) => ({
                    ...todo,
                    list: todo.list.filter((item: any) => item.status === this.status)
                }));
            }
            console.log("jsonData:", jsonData);
            return jsonData;
        } catch (error: any) {
            console.log("🚀  TodoListService  error:", error)
            throw error;
        }
    }
}
