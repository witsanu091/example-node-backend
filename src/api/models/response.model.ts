export interface Response {
    statusCode: number;
    developMessage: string;
    responseData: Array<any>;
}