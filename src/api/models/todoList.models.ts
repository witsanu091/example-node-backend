export interface List {
    listId: string;
    listName: string;
    status: string
}

export interface ToDoList {
    id: string;
    date: string;
    list: Array<List>;
};
