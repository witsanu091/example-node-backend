
import { TodoListService } from "../services/todoList.service";
import { Response } from "../models/response.model";
import { responseError, responseSuccess } from "../../utils/response"

export class TodoListController {

    async findAll() {
        console.log("controller findAll");
        const todoListService = new TodoListService({ id: "0", date: '', list: [] }, "");
        const result = await todoListService.readFileJson();
        console.log("🚀  TodoListController  result:", result)
        let responseResult: Response = {
            statusCode: responseError.statusCode,
            developMessage: responseError.developMessage,
            responseData: []
        };
        if (result) {
            responseResult.statusCode = responseSuccess.statusCode
            responseResult.developMessage = responseSuccess.developMessage
            responseResult.responseData = result
            return responseResult
        }
        return responseResult
    }

    async findByKey(key: any) {
        console.log("controller findByKey");
        const todoListService = new TodoListService({ id: key.id, date: key.date, list: key.list }, key.status);
        const result = await todoListService.getDataByKeyFromJson();
        console.log("🚀  TodoListController  result:", result)
        let responseResult: Response = {
            statusCode: responseError.statusCode,
            developMessage: responseError.developMessage,
            responseData: []
        };
        if (result) {
            responseResult.statusCode = responseSuccess.statusCode
            responseResult.developMessage = responseSuccess.developMessage
            responseResult.responseData = result
            return responseResult
        }
        return responseResult
    }
}
