import { TodoListController } from '../todoList.controller';
import { TodoListService } from '../../services/todoList.service';
import { Response } from '../../models/response.model';
import { responseError, responseSuccess } from '../../../utils/response';

jest.mock('../../services/todoList.service');

const dataTodoList = [{
    id: '1',
    date: '2024-02-29',
    list: [
        { listId: '101', listName: 'Grocery Shopping', status: 'progress' },
        { listId: '102', listName: 'Call Bank', status: 'progress' },
        {
            listId: '103',
            listName: 'Book Doctor Appointment',
            status: 'complete',
        },
    ],
}]


describe('TodoListController', () => {
    let todoListController: TodoListController;
    let mockTodoListService: jest.Mocked<TodoListService>;


    beforeEach(() => {
        mockTodoListService = new TodoListService({ id: "1", date: '', list: [] }, "") as any;
        todoListController = new TodoListController();
        jest.clearAllMocks();
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    describe('findAll', () => {
        test('should return success response if todo are found', async () => {

            const mockResponse: Response = {
                statusCode: 200,
                developMessage: 'Success',
                responseData: dataTodoList
            };

            TodoListService.prototype.readFileJson = jest.fn().mockResolvedValue(dataTodoList);

            const result = await todoListController.findAll();
            console.log("🚀  result:", result)

            expect(result).toEqual(mockResponse);
        });

        test('should return error response if no todo are found', async () => {
            TodoListService.prototype.readFileJson = jest.fn().mockResolvedValue(null);
            const result = await todoListController.findAll();

            expect(result.statusCode).toBe(responseError.statusCode);
            expect(result.developMessage).toBe(responseError.developMessage);
        });
    });

    describe('findByKey', () => {
        test('should return success response if todo is found by key', async () => {
            const mockResponse: Response = {
                statusCode: 200,
                developMessage: 'Success',
                responseData: dataTodoList
            };

            TodoListService.prototype.getDataByKeyFromJson = jest.fn().mockResolvedValue(dataTodoList);

            const result = await todoListController.findByKey({ id: '1' });

            expect(result).toEqual(mockResponse);
        });

        test('should return error response if todo is not found by key', async () => {

            TodoListService.prototype.getDataByKeyFromJson = jest.fn().mockResolvedValue(null);
            const result = await todoListController.findByKey({ id: '4' });

            expect(result.statusCode).toBe(responseError.statusCode);
            expect(result.developMessage).toBe(responseError.developMessage);
        });
    });
});
