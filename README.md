Set Up Node Back-End ( TypeScript ) 
1. Clone the repository using the command:
  - "git clone https://gitlab.com/witsanu091/example-node-backend.git"
2. Switch to the 'develop' branch with: 
  - "git switch develop"
3. Install necessary packages from 
  - "npm install"
4. Execute unit tests before running the application:
  - "npm test" 
5. Start the server for the REST API:
  - "npm run dev"
6. The server will start at:
  - http://localhost:3000 
  - http://127.0.0.1:3000/
7. Set up Postman to test API calls:
   7.1. Open Postman.
   7.2. Click the 'Import' button.
   7.3. Select and import the Postman collection file located in project at 
   - "example-node-backend/postman/Api Node Backend Example.postman_collection.json"
8. Interacting with Data through the API:
    8.1. Make calls to retrieve the entire todo list.
    8.2. Make calls to retrieve specific todo items by key (e.g., id, date, status).